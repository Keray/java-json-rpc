package pl.icedev.rpc.client;

public class RPCException extends RuntimeException {
    private int code;

    public RPCException(int code, String message) {
        super(message);
        this.code = code;
    }

    public RPCException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
