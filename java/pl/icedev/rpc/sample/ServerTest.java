package pl.icedev.rpc.sample;

import pl.icedev.rpc.server.JSONRPCHandler;
import pl.icedev.rpc.server.NIOServer;

import java.io.IOException;
import java.util.Map;


public class ServerTest {

    public static class WebServerInterface {
        public boolean checkUser(String userName) {
            return true;
        }

        public String createUser(String userName) {
            return userName + " test";
        }

        public void deleteUser(String userName) {
            if (userName == null)
                throw new RuntimeException("User not found");
        }

        public Object getVhost(String vhostName) {
            return null;
        }

        public void createVhost(String vhostName, String owner, Map<String, Object> options) {
            for (String key : options.keySet()) {
                System.out.println(key + ": " + options.get(key));
            }
            if (vhostName == null)
                throw new RuntimeException("VHost not found");
        }

        public void updateVhost(String vhostName, Map<String, Object> options) {
            if (vhostName == null)
                throw new RuntimeException("VHost not found");
        }

        public void deleteVhost(String vhostName) {
            if (vhostName == null)
                throw new RuntimeException("VHost not found");
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        WebServerInterface service = new WebServerInterface();
        JSONRPCHandler handler = new JSONRPCHandler(service, "test");
        NIOServer nios = new NIOServer("localhost", 6949, handler);

        System.out.println("Server running...");
        while (true) {
            nios.update(-1);
        }
    }

}
