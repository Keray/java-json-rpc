package pl.icedev.rpc.sample;

import pl.icedev.rpc.client.JSONRPC;

import java.io.IOException;

public class ClientTest {

    public static void main(String[] args) throws IOException {
        JSONRPC rpc = new JSONRPC("localhost", 6949);
        rpc.setAuthKey("test");

        Object resp = rpc.call("createUser", "Example");

        System.out.println(resp);

        rpc.close();
    }

}
