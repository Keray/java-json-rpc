package pl.icedev.rpc.server;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class NIOConnection {
    private static final int IPTOS_LOWCOST = 0x02;
    private static final int IPTOS_RELIABILITY = 0x04;
    private static final int IPTOS_THROUGHPUT = 0x08;
    private static final int IPTOS_LOWDELAY = 0x10;

    private SelectionKey key;
    private SocketChannel channel;
    private SocketAddress remote;

    ByteBuffer rbuff, wbuff;

    public NIOConnection(SocketChannel channel, Selector selector) throws IOException {
        channel.configureBlocking(false);

        Socket socket = channel.socket();
        socket.setTcpNoDelay(true);
        socket.setTrafficClass(IPTOS_LOWDELAY);
        socket.setKeepAlive(true);

        this.channel = channel;

        key = channel.register(selector, SelectionKey.OP_READ);
        key.attach(this);

        remote = channel.getRemoteAddress();

        rbuff = ByteBuffer.wrap(new byte[2048]);
        wbuff = ByteBuffer.wrap(new byte[1024 * 2]);

        rbuff.limit(0);
    }

    private final int readMore() throws IOException {
        SocketChannel socket = this.channel;
        if (socket == null)
            throw new SocketException("Connection is closed.");

        rbuff.compact();

        if (!rbuff.hasRemaining())
            throw new BufferOverflowException();
        int bytesRead = socket.read(rbuff);
        rbuff.flip();

        return bytesRead;
    }


    private int index = 0;

    public String readPacket() throws IOException {
        int read = readMore();
        if (read == -1) {
            if (rbuff.hasRemaining()) {
                int len = rbuff.limit();
                rbuff.limit(0);
                return new String(rbuff.array(), 0, len);
            }
            throw new SocketException("End of stream");
        }
        int end = rbuff.limit();
        int start = index;

        if (end == start)
            return null;

        byte[] arr = rbuff.array();

        boolean found = false;
        for (int i = start; i < end; i++) {
            if (arr[i] == '\n') {
                found = true;
                end = i;
                break;
            }
            index++;
        }

        if (found) {
            index = 0;
            String json = new String(arr, 0, end - start);
            rbuff.position(rbuff.position() + (end - start) + 1);
            return json;
        }

        return null;
    }

    void write() throws IOException {
        synchronized (wbuff) {
            wbuff.flip();
            channel.write(wbuff);
            if (!wbuff.hasRemaining()) {
                key.interestOps(SelectionKey.OP_READ);
            }
            wbuff.compact();
        }
    }

    @SuppressWarnings("unchecked")
    public void sendResponse(Object response, Object id) {
        JSONObject json = new JSONObject();
        json.put("result", response);
        json.put("id", id);
        send(json);
    }

    @SuppressWarnings("unchecked")
    public void sendError(int code, String message, Object id) {

        JSONObject error = new JSONObject();
        error.put("code", code);
        error.put("message", message);

        JSONObject json = new JSONObject();
        json.put("error", error);
        json.put("id", id);
        send(json);
    }

    public void send(JSONObject obj) {
        writePacket(obj.toJSONString());
    }

    public void writePacket(String p) {
        try {
            byte[] bytes = p.getBytes("UTF8");
            synchronized (wbuff) {
                wbuff.put(bytes);
                wbuff.put((byte) '\n');
                key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                write();
            }
        } catch (IOException e) {
            System.err.println(e);
            close();
        }
    }

    @Override
    public String toString() {
        return "Connection: " + remote;
    }

    public void close() {
        try {
            channel.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
