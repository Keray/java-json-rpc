package pl.icedev.rpc.server;

import org.json.simple.JSONObject;

public interface JSONHandler {
    public void handle(NIOConnection con, JSONObject json, Object id) throws Exception;
}
