package pl.icedev.rpc.server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NIOServer {
    private ServerSocketChannel channel;
    private Selector selector;

    List<NIOConnection> connections = new ArrayList<>();
    private final JSONHandler handler;
    private final JSONParser parser;

    public NIOServer(String host, int port, JSONHandler handler) throws IOException {
        selector = Selector.open();
        this.handler = handler;
        channel = selector.provider().openServerSocketChannel();
        channel.socket().bind(new InetSocketAddress(host, port));
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_ACCEPT);

        parser = new JSONParser();
    }

    private int select(int timeout) throws IOException {
        if (timeout == 0)
            return selector.selectNow();
        if (timeout == -1)
            return selector.select();
        return selector.select(timeout);
    }

    public void update(int timeout) throws IOException {
        int num = select(timeout);
        if (num == 0)
            return;

        Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

        while (keys.hasNext()) {
            SelectionKey key = keys.next();
            keys.remove();

            if (key.isAcceptable()) {
                SocketChannel accept = channel.accept();
                if (accept != null) {
                    accept.configureBlocking(false);
                    NIOConnection con = new NIOConnection(accept, selector);
                    connections.add(con);
                    System.out.println("Accepted connection: " + con);
                }
            }

            if (key.isReadable() || key.isWritable()) {
                NIOConnection con = (NIOConnection) key.attachment();
                if (con == null) {
                    key.channel().close();
                    continue;
                }
                try {
                    if (key.isReadable()) {
                        String packet;
                        while ((packet = con.readPacket()) != null) {
                            handlePacket(con, packet);
                        }
                    }
                    if (key.isWritable()) {
                        con.write();
                    }
                } catch (Exception e) {
                    connections.remove(con);
                    con.close();
                    if (!(e instanceof SocketException)) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void handlePacket(NIOConnection con, String packet) {
        // can be executed in another thread without huge problems
        Object object;
        try {
            object = parser.parse(packet);
        } catch (ParseException e) {
            con.sendError(-32700, e.getMessage(), null);
            return;
        }

        if (object instanceof JSONObject) {
            Object id = ((JSONObject) object).get("id");
            try {
                handler.handle(con, (JSONObject) object, id);
            } catch (Exception e) {
                e.printStackTrace();
                con.sendError(-32603, "Internal error", id);
            }
        } else {
            con.sendError(-32600, "Invalid request", null);
        }
    }
}
