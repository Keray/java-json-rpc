package pl.icedev.rpc.server;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.util.HashMap;

public class JSONRPCHandler implements JSONHandler {
    private String authKey;
    private Object target;
    private HashMap<String, Method> methods = new HashMap<>();

    public JSONRPCHandler(Object target) {
        this.target = target;
        this.authKey = null;

        Method[] meth = target.getClass().getMethods();
        for (Method m : meth) {
            methods.put(m.getName(), m);
        }
    }

    public JSONRPCHandler(Object target, String authKey) {
        this(target);
        this.authKey = authKey;
    }

    @Override
    public void handle(NIOConnection con, JSONObject json, Object id) throws Exception {
        Object methodObj = json.get("method");
        if (methodObj == null || !(methodObj instanceof String)) {
            con.sendError(-32600, "Method item not provided in request object.", id);
            return;
        }

        String method = (String) json.get("method");
        Method m = methods.get(method);
        if (m == null) {
            con.sendError(-32601, "Method not found.", id);
            return;
        }

        if (!json.containsKey("params") || !(json.get("params") instanceof JSONArray)) {
            con.sendError(-32600, "Params item is not present in request object.", id);
            return;
        }

        if (authKey != null) {
            Object userAuthKeyObj = json.get("authKey");
            if (userAuthKeyObj == null || !(userAuthKeyObj instanceof String)) {
                con.sendError(-32001, "Authentication via authKey required.", id);
                return;
            }

            if (!userAuthKeyObj.equals(authKey)) {
                con.sendError(-32001, "Invalid authKey.", id);
                return;
            }
        }

        Object[] params = ((JSONArray) json.get("params")).toArray();
        Class<?>[] types = m.getParameterTypes();
        if (params.length != types.length) {
            con.sendError(-32602, "Expected " + types.length
                    + " parameters instead of " + params.length, id);
            return;
        }

        for (int i = 0; i < params.length; i++) {
            Class<?> type = types[i];

            // json parser uses Double and Long by default
            if (type == Float.class || type == float.class) {
                params[i] = toFloat(params[i]);
            } else if (type == Integer.class || type == int.class) {
                params[i] = toInt(params[i]);
            }
        }
        try {
            Object result = m.invoke(target, params);
            con.sendResponse(result, id);
        } catch (InvocationTargetException e) {
            if (e.getCause() == null) {
                throw e;
            }
            con.sendError(-32603, pretty(e.getCause()), id);
        }
    }

    private Integer toInt(Object param) {
        if (param instanceof Integer)
            return (Integer) param;
        if (param instanceof Long)
            return ((Long) param).intValue();
        throw new InvalidParameterException(param + "");
    }

    private Float toFloat(Object param) {
        if (param instanceof Float)
            return (Float) param;
        if (param instanceof Double)
            return ((Double) param).floatValue();
        throw new InvalidParameterException(param + "");
    }

    private String pretty(Throwable e) {
        String name = e.getClass().getSimpleName();
        String message = e.getMessage();
        if (message == null)
            return name;
        return name + ": " + message;
    }

    private String type(Object param) {
        return param == null ? "" : param.getClass().getName();
    }
}
