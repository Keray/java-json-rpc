<?php
header('Content-Type: text/html; charset=utf-8');

class JSONRPC {
	var $sock;
	var $id = 1;
	function __construct($host, $port) {
		$this->sock = @fsockopen($host, $port, $errno, $errstr) or die("Unable to connęct, code: " . $errno);
	}
	
	function __call($name, $args) {
		$json = [];
		
		$json['method'] = $name;
		$json['params'] = $args;
		$json['id'] = $this->id++;
		
		fputs($this->sock, json_encode($json) . "\n");
		fflush($this->sock);
		
		$str = fgets($this->sock, 2048);
		$result = json_decode($str, true);
		
		if(array_key_exists('response', $result)) {
			return $result['response'];
		}
		if(isset($result['error']['message']))
			throw new Exception($result['error']['message']);
		throw new Exception($str);
	}
	function __toString() {
		return "[TCP connection]";
	}
	function close() {
		fclose($this->sock);
	}
}

try {
	$rpc = new JSONRPC('127.0.0.1', 6669);

	$map = array();
	$map['szatan'] = 666;
	$map['monk'] = "Jak to zrobic nie wiem";

	var_dump($rpc->createVhost("hrhr","gunwo", $map));
	echo '<br><br>';
	var_dump($rpc->checkUser("kerai"));
	echo '<br><br>';
	var_dump($rpc->createUser("Wałęsa"));
	echo '<br><br>';
	var_dump($rpc->deleteUser(null));

} catch(Exception $ex) {
	echo get_class($ex) .': <b>' . $ex->getMessage() .'</b>';
	echo '<pre>' . $ex->getTraceAsString() . '</pre>';
}


$rpc->close();

